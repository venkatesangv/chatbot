import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms'; 
import { ChatbotService } from './service/chatbot.service';
import { ChatDialogComponent } from './chat-dialog/chat-dialog.component';
import { PortfolioListComponent } from './portfolio-list/portfolio-list.component'; 
@NgModule({
  imports: [
    CommonModule,
    FormsModule 
  ],
  declarations: [ChatDialogComponent, PortfolioListComponent],
  exports:[ChatDialogComponent],
  providers: [ChatbotService]
})
export class ChatbotModule { }
