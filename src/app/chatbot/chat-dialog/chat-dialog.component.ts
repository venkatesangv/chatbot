import { Component, OnInit } from '@angular/core';
import { Message, MessageEnum, IWindow } from '../module/message';
import { ChatbotService } from 'app/chatbot/service/chatbot.service';
declare var $: any;

@Component({
  selector: 'chat-dialog',
  templateUrl: './chat-dialog.component.html',
  styleUrls: ['./chat-dialog.component.css']
})
export class ChatDialogComponent implements OnInit {
  formValue:string;
  messages: Message[]= new Array<Message>();
  messageContent: string;
  
  constructor(private chatbotService:ChatbotService) {
    this.messages.push({content:"Hi, how may i assist you?", sentBy: MessageEnum.BOT, timeStamp: new Date().toLocaleTimeString() });
    }

  ngOnInit() {
  }

  public sendMessage(): void {
    this.messages.push({content:this.messageContent, sentBy: MessageEnum.USER, timeStamp: new Date().toLocaleTimeString() });
    this.chatbotService.getResponse(this.messageContent).subscribe(res => {
       this.messageContent ="";
       if(res.result.action === "input.stock_suggestion" && !res.result.actionIncomplete){
        this.messages.push({showPortfolioList :{date: res.result.parameters.date, userId:1234}, sentBy: MessageEnum.BOT, timeStamp: new Date().toLocaleTimeString() });
        
       }else {
        this.messages.push({content:res.result.fulfillment.speech, sentBy: MessageEnum.BOT, timeStamp: new Date().toLocaleTimeString() });
       }
        var objDiv = $(".panel-body");
        var h = objDiv.get(0).scrollHeight;
        objDiv.animate({scrollTop: h});
      });
  }

  public googleSpeech(){
    if (window.hasOwnProperty('webkitSpeechRecognition')) {
      const {webkitSpeechRecognition} : IWindow = <IWindow>window;      
      let recognition = new webkitSpeechRecognition();
      recognition.continuous = false;
      recognition.interimResults = false;
      recognition.lang = "en-US";
      recognition.start();
      recognition.onresult = function(e) {
        this.messageContent = e.results[0][0].transcript;
        recognition.stop(); 
      };
      recognition.onerror = function(e) {
        recognition.stop();
      }
    }
  }
}
