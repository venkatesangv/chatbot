export class Message {
    content?: string;
    sentBy: number;
    timeStamp: string;
    showPortfolioList?: PortfolioList;
}

export class PortfolioList {
    userId: number;
    date: number;
}

export enum MessageEnum{
    USER,
    BOT
}

export interface IWindow extends Window {
    webkitSpeechRecognition: any;
    SpeechRecognition: any;
  }