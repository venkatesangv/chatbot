import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-portfolio-list',
  templateUrl: './portfolio-list.component.html',
  styleUrls: ['./portfolio-list.component.css']
})
export class PortfolioListComponent implements OnInit {
@Input() userId;
@Input() date;
  constructor() { }

  ngOnInit() {
  }

}
